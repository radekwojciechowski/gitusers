package pl.codev.gitusers

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.main_activity.*
import pl.codev.gitusers.common.GitUsersAdapter
import pl.codev.gitusers.common.GitUsersPreferences
import pl.codev.gitusers.data.models.GitUserModel
import pl.codev.gitusers.data.services.GitUsersService
import java.net.UnknownHostException
import javax.inject.Inject

/**
 * Created by Radek on 2020-04-23.
 */
class MainActivity : AppCompatActivity() {

    companion object {
        private val TAG: String = MainActivity::class.java.simpleName
    }

    var disposable: Disposable? = null
    var gitUsers: ArrayList<GitUserModel.GitUser>? = null

    @Inject lateinit var gitUsersPreferences: GitUsersPreferences
    @Inject lateinit var gitUsersService: GitUsersService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        (application as GitUsersApplication).gitUsersComponent.inject(this)

        setupUI()

        val listType = object : TypeToken<ArrayList<GitUserModel.GitUser>>() { }.type
        gitUsers = Gson().fromJson<ArrayList<GitUserModel.GitUser>>(gitUsersPreferences.getString(GitUsersPreferences.GITHUB_USERS, ""), listType)

        getUsers()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }

    private fun getUsers() {
        Log.d(TAG, "getUsers")

        disposable = gitUsersService.getAllUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                {
                    if (it.isSuccessful) {
                        it.body()?.let {
                            Log.d(TAG, "Response size: " + it.size)

                            if (it.size > 0) {
                                gitUsers = ArrayList(it.take(30))
                                getUserRepos(0)
                            } else {
                                errorCapture(getString(R.string.no_data_label))
                            }
                        }
                    } else {
                        Log.e(TAG, "Data read error: " + it.message())
                        if (it.code() == 403 && it.headers().get("X-RateLimit-Remaining").equals("0"))
                            showData()
                        else
                            errorCapture(it.message())
                    }


                },
                {
                    Log.e(TAG, "onError: " + it.message)
                    it.printStackTrace()
                    if (it is UnknownHostException) {
                        gitUsers?.let {
                            Log.d(TAG, "Users data size: " + it.size)
                            if (it.isNotEmpty())
                                showData()
                            else
                                errorCapture(getString(R.string.missing_net_message))

                        } ?: run {
                            errorCapture(getString(R.string.missing_net_message))
                        }

                    } else
                        errorCapture(it.message)
                }
            )

    }

    private fun getUserRepos(index: Int) {
        Log.d(TAG, "getUserRepos")

        gitUsers?.let {
            if (index < it.size) {
               it[index]?.let {
                   var gitUser = it
                   disposable = gitUsersService.getUserRepos(it.login)
                       .subscribeOn(Schedulers.io())
                       .observeOn(AndroidSchedulers.mainThread())
                       .subscribe(
                           {
                               if (it.isSuccessful) {
                                   it.body()?.let {
                                       Log.d(TAG, "Response size repos: " + it.size)
                                       gitUser.repos = it.take(3)

                                       gitUsers?.let {
                                           it.add(gitUser)
                                           gitUsersPreferences.setString(GitUsersPreferences.GITHUB_USERS, Gson().toJson(it))
                                       }

                                       getUserRepos(index.inc())
                                   } ?: run {
                                       errorCapture(null)
                                   }
                               } else {
                                   Log.e(TAG, "Data read error: " + it.message())
                                   if (it.code() == 403 && it.headers().get("X-RateLimit-Remaining").equals("0"))
                                       showData()
                                   else
                                       errorCapture(it.message())
                               }

                           },
                           {
                               Log.e(TAG, "onError: " + it.message)
                               it.printStackTrace()
                               errorCapture(it.message)
                           }
                       )
               }
           } else {
               Log.d(TAG, "Data reading finished")
               showData()
           }
        }

    }

    private fun errorCapture(errorMessage: String?) {
        errorMessage?.let {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            message_text_view.setText(it)
        } ?: run {
            Toast.makeText(this, getString(R.string.unexpected_error_message), Toast.LENGTH_LONG).show()
            message_text_view.setText(getString(R.string.unexpected_error_message))
        }

        refresh_button.visibility = View.VISIBLE
    }

    private fun setupUI() {
        refresh_button.setOnClickListener {
            message_text_view.setText(getString(R.string.wait_label))
            refresh_button.visibility = View.INVISIBLE
            getUsers()
        }

        search_button.setOnClickListener {
            if (search_text.text.length < 3)
                Toast.makeText(this, getString(R.string.search_not_enough_message), Toast.LENGTH_LONG).show()
            else {
                gitUsers?.let {
                    val filteredGitUsers = ArrayList(it.filter {
                        it.login.contains(search_text.text, true) ||
                                (it.repos != null && (it.repos.filter {it.name.contains(search_text.text, true)}).isNotEmpty())
                    })
                    git_users_list.adapter = GitUsersAdapter(this, filteredGitUsers)
                }

            }
        }

        reset_button.setOnClickListener {
            search_text.setText("")
            showData()
        }
    }

    private fun showData() {
        Log.d(TAG, "showData")
        data_read_view.visibility = View.GONE
        data_show_view.visibility = View.VISIBLE

        gitUsers?.let {
            git_users_list.adapter = GitUsersAdapter(this, it)
        }

    }
}