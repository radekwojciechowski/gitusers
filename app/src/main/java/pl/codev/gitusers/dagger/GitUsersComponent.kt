package pl.codev.gitusers.dagger

import dagger.Component
import pl.codev.gitusers.MainActivity
import javax.inject.Singleton

/**
 * Created by Radek on 2020-04-23.
 */
@Singleton
@Component(modules = [GitUsersModule::class])
interface GitUsersComponent {
    fun inject(act: MainActivity)
}