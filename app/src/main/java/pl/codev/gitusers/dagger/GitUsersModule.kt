package pl.codev.gitusers.dagger

import android.app.Application
import android.content.Context
import android.util.Log
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.codev.gitusers.common.GitUsersPreferences
import pl.codev.gitusers.data.services.GitUsersService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Radek on 2020-04-23.
 */
@Module
class GitUsersModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideGitUsersPreferences(context: Context): GitUsersPreferences {
        return GitUsersPreferences(context)
    }

    @Provides
    @Singleton
    fun provideGitUsersService(): GitUsersService {
        val loggingInterceptor = HttpLoggingInterceptor {
            Log.i("retrofit", it)
        }

        val client = OkHttpClient().newBuilder()
            .addInterceptor(loggingInterceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(
                RxJava2CallAdapterFactory.create())
            .addConverterFactory(
                GsonConverterFactory.create())
            .baseUrl("https://api.github.com/")
            .client(client)
            .build()

        return retrofit.create(GitUsersService::class.java)
    }
}