package pl.codev.gitusers.common

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Radek on 2020-04-23.
 */
class GitUsersPreferences(context: Context) {
    var sharedPreferences: SharedPreferences? = null

    companion object {
        private val SH_NAME = "TEST_APP"

        var GITHUB_USERS = "github_users"
    }

    init {
        sharedPreferences = context.getSharedPreferences(SH_NAME, 0)
    }

    fun setString(key: String, value: String?) {
        sharedPreferences?.let {
            val editor = it.edit()
            editor.putString(key, value)
            editor.apply()
        }
    }

    fun getString(key: String, defaultValue: String?): String? {
        return sharedPreferences?.let {
            return it.getString(key, defaultValue)
        }
    }
}