package pl.codev.gitusers.common

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import pl.codev.gitusers.R
import pl.codev.gitusers.data.models.GitUserModel

/**
 * Created by Radek on 2020-04-23.
 */
class GitUsersAdapter(private val context: Context,
                      private val dataSource: ArrayList<GitUserModel.GitUser>): BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.git_user_list_item, parent, false)

        Picasso.with(context)
            .load(dataSource[position].avatarUrl)
            .into((rowView.findViewById(R.id.git_user_avatar) as ImageView))

        (rowView.findViewById(R.id.git_user_login) as TextView).text = dataSource[position].login
        var repos: String = String()
        if (dataSource[position].repos != null) {
            for (repo in dataSource[position].repos) {
                repos += repo.name + "\n";
            }
        }
        (rowView.findViewById(R.id.git_user_repos) as TextView).text = repos

        return rowView
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }
}