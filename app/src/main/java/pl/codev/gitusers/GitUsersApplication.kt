package pl.codev.gitusers

import android.app.Application
import pl.codev.gitusers.common.GitUsersPreferences
import pl.codev.gitusers.dagger.DaggerGitUsersComponent
import pl.codev.gitusers.dagger.GitUsersComponent
import pl.codev.gitusers.dagger.GitUsersModule

/**
 * Created by Radek on 2020-04-23.
 */
class GitUsersApplication: Application() {

    lateinit var gitUsersComponent: GitUsersComponent

    override fun onCreate() {
        super.onCreate()

        gitUsersComponent = DaggerGitUsersComponent.builder()
            .gitUsersModule(GitUsersModule(this))
            .build()
    }
}