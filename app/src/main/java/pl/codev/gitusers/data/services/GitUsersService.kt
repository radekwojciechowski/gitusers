package pl.codev.gitusers.data.services

import io.reactivex.Observable
import pl.codev.gitusers.data.models.GitUserModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Radek on 2020-04-23.
 */
interface GitUsersService {

    @GET("users")
    fun getAllUsers(): Observable<Response<List<GitUserModel.GitUser>>>

    @GET("users/{user_login}/repos")
    fun getUserRepos(@Path("user_login") userLogin: String): Observable<Response<List<GitUserModel.GitUserRepo>>>
}