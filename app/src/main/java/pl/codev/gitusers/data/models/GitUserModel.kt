package pl.codev.gitusers.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Radek on 2020-04-23.
 */
object GitUserModel {
    data class GitUserRepo(val name: String)
    data class GitUser(val login: String, @SerializedName("avatar_url") val avatarUrl: String) {
        var repos: List<GitUserRepo> = listOf<GitUserModel.GitUserRepo>()
    }
}